package rtx;

import java.io.Serializable;

public class RtxRetObj<T > implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1091666174868317933L;
	int innerCode;
	String resultErr;
	T data;
	public int getInnerCode() {
		return innerCode;
	}
	public void setInnerCode(int innerCode) {
		this.innerCode = innerCode;
	}
	public String getResultErr() {
		return resultErr;
	}
	public void setResultErr(String resultErr) {
		this.resultErr = resultErr;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
	
	
}
