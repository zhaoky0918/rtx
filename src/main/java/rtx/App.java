package rtx;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		try {
			IRTXSvrApi irtxSvrApi = new RTXSvrApi();
			LocateRegistry.createRegistry(8084);
			Naming.bind("rmi://localhost:8084/rtxSvrApi", irtxSvrApi);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("rmi server is starting...");
	}

}
