package rtx;


import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 封装对RTX的操作
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2007</p>
 * <p>Company: Tencent C0. Ltd</p>
 * @author herolin
 * @version 1.0
 */
public interface IRTXSvrApi extends Remote {



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                         系统自带的函数                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 初始化类
	public boolean init()throws RemoteException;

	// 析构类
	public void unInit()throws RemoteException;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //                                           扩展的函数                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    

   /**
    * 设置服务器IP
    * @param strIP String
    */
   public int setServerIP(String strIP) throws RemoteException;

   /**
    * 设置服务器端口
    * @param iPort int 端口，默认是6000
    */
   public int setServerPort(int iPort)throws RemoteException;

   /**
    * 取服务器IP
    *@param iPort int
    */
   public  String getServerIP()throws RemoteException;

   /**
    * 取服务器端口
    * @return String 服务器地址
    */
   public  int getServerPort()throws RemoteException;

    /**
     * 查询用户在线状态
     * @param UserName String 用户帐号
     * @return int 0:离线 1:在线 2:离开 11:不存在该用户 其它:未知状态
     */
    public  int QueryUserState(String UserName)throws RemoteException;


    /**
     * 删除用户信息
     * @param String UserName用户帐号
     * @return int  0 操作成功 非0为失败
     */
    public  RtxRetObj<?> deleteUser(String UserName)throws RemoteException;


    /**
     * 新增用户
     * @param UserName string 用户帐号
     * @param DeptID String 部门ID	
     * @paramint ChsName String 用户姓名
     * @paramint Pwd String 密码
     * @return int  0 操作成功 非0为失败
     */
    public  RtxRetObj<?> addUser(String UserName, String DeptID, String ChsName, String Uin, String Pwd )throws RemoteException;

    /**
     * 查看用户简单资料
     * @param UserName String 用户帐号
     * @return String[][]  操作成功返回一个二维数组 失败为null
      */
	public  String[][] GetUserSimpleInfo(String UserName)throws RemoteException;
	
	/**
     * 设置用户简单资料
     * @param UserName String 用户帐号
     * @param ChsName String 用户姓名
     * @param email String 邮箱地址
     * @param gender String 性别，男为0，女为1
     * @param mobile String 手机号码
     * @param phone String  电话
     * @param pwd String 密码
     * @return int  0 操作成功 非0为失败
      */
	public  RtxRetObj<?> SetUserSimpleInfo(String UserName,String ChsName,String email,String gender,String mobile,String phone,String pwd)throws RemoteException;
	
	
	/**
     * 设置用户简单资料
     * @param UserName String 用户帐号
     * @param pwd String 密码
     * @return int  0 操作成功 非0为失败
      */
	public  int SetUserPassword(String UserName,String pwd)throws RemoteException;

	/**
     * 设置用户简单资料，支持设置部门ID
     * @param UserName String 用户帐号
     * @param DeptID String 部门ID
     * @param ChsName String 用户姓名
     * @param email String 邮箱地址
     * @param gender String 性别，男为0，女为1
     * @param mobile String 手机号码
     * @param phone String  电话
     * @param pwd String 密码
     * @return int  0 操作成功 非0为失败
      */
	public  RtxRetObj<?> SetUserSimpleInfoEx(String UserName,String DeptID, String ChsName,String email,String gender,String mobile,String phone,String pwd)throws RemoteException;
	
    /**
     * 查看用户详细资料
     * @param UserName String 用户帐号
     * @return String[][] 操作成功返回一个二维数组 失败返回null
     */
    public  String[][] GetUserDetailInfo(String UserName)throws RemoteException;
    
    /**
     * 更新用户信息
     * @param UserName String
     * @param ....... String
     * @param MOBILE String
     * @return int  0 操作成功 非0为失败
     */
    public  int setUserDetailInfo(String UserName,String ADDRESS,String AGE,String BIRTHDAY,
    String BLOODTYPE,String CITY,String COLLAGE,String CONSTELLATION,String COUNTRY,String FAX,
    String HOMEPAGE,String MEMO,String POSITION,String POSTCODE,String PROVINCE,String STREET,
    String PHONE,String MOBILE)throws RemoteException;

    /**
     * 把RTX号码转换为呢称
     * @param  UIN String RTX号码
     * @return int  0 操作成功 非0为失败
     */
	public  String UinToUserName ( String UIN)throws RemoteException;
	
    /**
     * 添加组织信息
     * @param deptId String		部门ID
     * @param DetpInfo String	部门信息
     * @param DeptName String	部门名称
     * @param ParentDeptId String 	父部门ID
     * @param type String 	0:只删除当前组织 1:删除下级组织及用户
     * @return int  0 操作成功 非0为失败
     */
    public  RtxRetObj<?> addDept(String deptId,String DetpInfo,String DeptName,String ParentDeptId,String SortId )throws RemoteException;

    /**
     * 修改组织信息
     * @param deptId String 	部门ID
     * @param DetpInfo string 	部门信息
     * @param DeptName string 	部门名称
     * @param ParentDeptId string	父部门ID
     * @return int  0 操作成功 非0为失败
     */
    public  RtxRetObj<?> setDept(String deptId,String DetpInfo,String DeptName,String ParentDeptId, String sortId )throws RemoteException;

    /**
     * 删除组织信息
     * @param deptId String	部门
     * @param type String 0:只删除当前组织 1:删除下级组织及用户
     * @return int  0 操作成功 非0为失败
     */
    public  RtxRetObj<?> deleteDept(String deptId,String type)throws RemoteException;

    /**
     * 判断某个组织是否存在
     * @param deptId String 部门ID
     * @return int 0:存在 非0:不存在
     */
    public  int deptIsExist(String deptId)throws RemoteException;

    /**
     * 取当前组织下的用户
     * @param DeptID String 部门ID
     * @return String[] 成功返回部门下用户帐号数组，失败返回null
     */
    public  String[] getDeptUsers(String DeptID)throws RemoteException;


    /**
     * 取当前组织下的子部门ID
     * @param DeptID String 部门ID
     * @return String[] 成功返回子部门数组，失败返回null
     */
    public  String[]  getChildDepts(String DeptID)throws RemoteException;

    /**
     * 判断某个用户是否存在
     * @param UserName String 用户帐号
     * @return int 0:存在 非0:不存在
     */
    public  RtxRetObj<Boolean> userIsExist(String UserName)throws RemoteException;

   /**
    * 获取sessionKey
    * @param UserName String 用户帐号
    * @return String 成功返回SessionKey，失败返回null
    */
   public  String getSessionKey(String UserName)throws RemoteException;

   
   /**
     * 发送消息提醒
     * @param receivers String 接收人(多个接收人以逗号分隔)
     * @param title String 消息标题
     * @param msg String 消息内容
     * @param type String 0:普通消息 1:紧急消息
     * @param delayTime String 显示停留时间(毫秒) 0:为永久停留(用户关闭时才关闭)
     * @return int 0:操作成功 非0:操作不成功
     */
    public  int sendNotify(String receivers,String title,String msg, String type,String delayTime)throws RemoteException;

    /**
    * 发送短信
    * @param sender String 发送人
    * @param receiver String 接收人(RTX用户或手机号码均可,最多128个)
    * @param smsInfo String 短信内容
    * @param autoCut int 是否自动分条发送 0:否 1:是
    * @param noTitle int 是否自动填写标题 0:自动 1:制定
    * @return int 成功返回0，失败返回其他
    */
   public  int sendSms(String sender, String receiver, String smsInfo,int autoCut, int noTitle)throws RemoteException;
   
    /**
    * 导出rtx的用户数据为xml
    * @return string  成功返回RTX用户数据的xml,失败返回null
    */
	public  String exportXmldata()throws RemoteException;

	/**
    * 导入xml的用户数据
    * @param xmldata String xml用户数据
    * @return int :成功返回部门名称，失败返回null
    */
	public  int  importXmldata(String xmldata)throws RemoteException;

	/**
    * 获取部门名称
    * @param deptID String 部门ID
    * @return String 0:操作成功 非0:操作不成功
    */	
	public  String  GetDeptName(String deptID)throws RemoteException;


	/**
    * 获取部门信息
    * @param deptID String 部门ID
    * @return String 0:操作成功 非0:操作不成功
    */	
	public  String[][]  GetDeptSimpleInfo(String deptID)throws RemoteException;
	
	
	/**
    * 获取部门信息
    * @param deptID String 部门ID
    * @return String 0:操作成功 非0:操作不成功
    */	
	public  String[][]  SearchUsers(String str)throws RemoteException;
}
